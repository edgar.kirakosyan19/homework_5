#  homework 1
print("Homework 1")

length = int(input("Tell me length: "))

i = 1
up = True
while i > 0 and length > 1:
    print("*"*i)

    if i == length:
        up = False

    if up:
        i += 1
    else:
        i -= 1

# homework 2
print("Homework 2")

for j in range(1,10):
    if j == 1:
        prev = 1
    else:
        prev = j-1

    print(f"{prev} x {j} = {j*prev}")

# homework 3
print("Homework 3")

number = int(input("tell number for divisor: "))

i = 1
while i <= number:
    if (number % i==0):
        print(i),
    i = i + 1

# homework 4
print("Homework 4")
number = int(input("tell number for factorial: "))
factorial = 1
for f in range(1, number+1):
    factorial *= f

print(factorial)

# homework 5
print("Homework 5")
number = int(input("tell number for fibonacci: "))
i = 0
item1 = 0
item2 = 1

while i < number:
    print(item1)
    sum_ = item1 + item2
    item1 = item2
    item2 = sum_
    i += 1